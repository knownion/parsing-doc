import re
import json
from flask import request, Flask

app = Flask(__name__)

def header_to_json(data):
    data = re.search(r"<!--.*-->", data, re.DOTALL).group(0)
    data = data[4:-3]
    data = json.loads(data)
    return data


def delete_backtick_code(data):
    _data = ""
    switch = False
    for l in data.splitlines():
        if('```' in l):
            switch = not switch
        elif(not switch):
            _data += l + "\n"
    return _data


def get_titles_1(data):
    """
        this function return
        only titles of type <h1>
    """
    data = delete_backtick_code(data)
    titles = re.findall(r"^\# .*$", data, flags=re.MULTILINE)
    # Second replace clause is useless because the only 1 #
    # It's maybe for future improve
    titles = [x.replace('# ', '').replace('#', '') for x in titles]
    return titles


def check_errors(js):
    if len(js["keywords"]) < 1:
        raise Exception("Not conform - Doesn't contain keywords")
    if len(js["titles_1"]) < 1:
        raise Exception("Not conform - Doesn't contain titles_1")


def parse(data):
    js = header_to_json(data)
    titles = get_titles_1(data)
    js["titles_1"] = titles
    check_errors(js)

    return js



@app.route('/', methods=['POST'])
def main():
    js = None
    try:
        data = request.form['data']
        js = parse(data)
    except Exception as err:
        return "Error: " + str(err), 400
    
    return js

