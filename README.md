# Parsing Doc

A set of small functions to parse markdown document. 
<br/>It's in fact an API for cloud function

* This app listen on the path `/parse` 
* Arguments must be sent with **POST method**
* Should send the **raw data** with a form 
  * This form should have the **field `data`**

A script `startdev` start a flask server on 0.0.0.0:5000 with the **dev mode**.