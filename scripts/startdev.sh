#!/bin/bash

###############
## This script is design to work in debug mode 
## on local machine, it's not intended 
## to work with docker. 
###############

export FLASK_APP=app.py
export FLASK_ENV=development
dirpath=$(dirname $(which $0))

cd "$dirpath"/..

flask run --host 0.0.0.0 --port 5000
